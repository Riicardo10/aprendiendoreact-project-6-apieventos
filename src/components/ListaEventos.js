import React, { Component } from 'react';
import Evento from './Evento';
import PropTypes from 'prop-types';

class ListaEventos extends Component {
    render() {
        return (
            <div className='uk-child-width-1-4@m' uk-grid='true'>
                { Object.keys( this.props.eventos ).map( evento => {
                    return <Evento key={evento} evento={this.props.eventos[evento]}/>;
                } ) }
            </div>
        );
    }
}

ListaEventos.propTypes = {
    eventos: PropTypes.array.isRequired
}

export default ListaEventos;