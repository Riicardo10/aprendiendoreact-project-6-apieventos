import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Formulario extends Component {

    evento = React.createRef();
    categoria = React.createRef();

    buscarEvento = (e) => {
        e.preventDefault();
        let evento = this.evento.current.value;
        let categoria = this.categoria.current.value;
        let data = {
            evento,
            categoria
        }
        this.props.getEventos( data );
    }

    render() {
        // const categorias = this.props.categorias;
        // console.log(categorias)
        return (
            <form onSubmit={this.buscarEvento}>
                <fieldset className='uk-fieldset uk-margin'>
                    <legend className='uk-legend uk-text-center'>
                        Busca tu evento por nombre o categoría
                    </legend>
                    <div className='uk-column-1-3@m uk-margin'>
                        <div className='uk-margin' uk-margin='true'>
                            <input ref={this.evento} className='uk-input' type='text' placeholder='Nombre de evento o ciudad.' />
                        </div>
                        <div className='uk-margin' uk-margin='true'>
                            <select ref={this.categoria} className='uk-select'>
                                {Object.keys( this.props.categorias).map( categoria => {
                                    return  <option key={categoria} value={this.props.categorias[categoria].id}> 
                                                {this.props.categorias[categoria].name_localized} 
                                            </option>
                                } ) }
                            </select>
                        </div>
                        <div className='uk-margin' uk-margin='true'>
                            <input type='submit' className='uk-button uk-button-danger' value='Buscar' />
                        </div>
                    </div>
                </fieldset>
            </form>
        );
    }
}

Formulario.propTypes = {
    categorias: PropTypes.array.isRequired
}

export default Formulario;