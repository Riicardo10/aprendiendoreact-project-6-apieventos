import React, { Component } from 'react';
import Header from './Header';
import Formulario from './Formulario';
import ListaEventos from './ListaEventos';

class App extends Component {

  TOKEN = 'DMXDBPI6JMDIQJZT3TWL';

  state = {
    categorias: [],
    eventos: []
  }

  componentDidMount() {
    this.getCategorias();
  }

  getCategorias = async () => {
    let url = 'https://www.eventbriteapi.com/v3/categories/?token=' + this.TOKEN + '&locale=es_ES';
    await fetch( url )
      .then( res => {
        return res.json();
      } )
      .then( categorias => {
        this.setState( {categorias: categorias.categories} );
      } )
  }

  getEventos = async (data) => {
    let url = 'https://www.eventbriteapi.com/v3/events/search/?q=' + data.evento + '&sort_by=date&categories=' + data.categoria + '&token=' + this.TOKEN + '&locale=es_ES'
    await fetch( url )
      .then( res => {
        return res.json();
      } )
      .then( eventos => {
        this.setState( {eventos: eventos.events} );
      } )
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className='uk-container'>
          <Formulario 
            categorias={this.state.categorias}
            getEventos={this.getEventos}/>
          <ListaEventos 
            eventos={this.state.eventos} />          
        </div>
      </div>
    );
  }
}

export default App;
