import React from 'react';
import PropTypes from 'prop-types';

const Evento = (props) => {
    const {name} = props.evento;
    if( !name ) return null;
    let titulo = name.text;
    if( titulo.length > 40 ) {
        titulo = titulo.substring(0, 40);
    }
    let descripcion = props.evento.description.text;
    if( descripcion.length > 120 ) {
        descripcion = descripcion.substring(0, 120) + '... (ver más)';
    }
    return (
        <div>
            <div className='uk-card uk-card-default'>
                <div className='uk-card-media-top'>
                    {props.evento.logo !== null 
                        ? <img src={props.evento.logo.url} alt={props.evento.name.text} /> 
                        : '' }    
                </div>
                <div className='uk-card-body'>
                    <h3 className='uk-card-title'> {titulo} </h3>
                    <p> {descripcion} </p>
                </div>
                <div className='uk-card-footer'>
                    <a className='uk-button uk-button-secondary' href={props.evento.url} target='_blank'> Más informacion</a>
                </div>
            </div>
        </div>
    );
}

Evento.propTypes = {
    evento: PropTypes.object.isRequired
}

export default Evento;